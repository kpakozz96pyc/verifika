import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';
import {HeaderComponent} from "./grid/header.component";
import {SideComponent} from "./grid/side.component";
import {ContentComponent} from "./grid/content.component";
import {FooterComponent} from "./grid/footer.component";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {
  MdButtonModule, MdCheckboxModule, MdToolbarModule,
  MdIconModule, MdListModule, MdCardModule
} from '@angular/material';

@NgModule({
  imports:      [
    BrowserModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MdButtonModule,
    MdCheckboxModule,
    MdToolbarModule,
    MdIconModule,
    MdListModule,
    MdCardModule
  ],
  declarations: [ AppComponent , HeaderComponent, SideComponent, ContentComponent, FooterComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
