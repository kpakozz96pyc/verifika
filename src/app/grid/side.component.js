"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var SideComponent = (function () {
    function SideComponent() {
        this.folders = [
            {
                name: 'Dashboard',
            },
            {
                name: 'User Profile',
            },
            {
                name: 'Workspace',
            }
        ];
        this.notes = [
            {
                name: 'Vacation Itinerary',
                updated: new Date('2/20/16'),
            },
            {
                name: 'Kitchen Remodel',
                updated: new Date('1/18/16'),
            }
        ];
    }
    return SideComponent;
}());
SideComponent = __decorate([
    core_1.Component({
        selector: 'v-side',
        template: "\n<md-card class=\"sidebar\">\n    <md-list>\n  <h3 md-subheader>My workspace</h3>\n  <md-list-item *ngFor=\"let folder of folders\">\n    <md-icon md-list-icon>folder</md-icon>\n    <h4 md-line>{{folder.name}}</h4>\n  </md-list-item>\n  <h3 md-subheader>Notes</h3>\n  <md-list-item *ngFor=\"let note of notes\">\n    <md-icon md-list-icon>note</md-icon>\n    <h4 md-line>{{note.name}}</h4>\n  </md-list-item>\n</md-list>\n</md-card>"
    })
], SideComponent);
exports.SideComponent = SideComponent;
//# sourceMappingURL=side.component.js.map