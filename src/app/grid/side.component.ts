import { Component } from '@angular/core';

@Component({
  selector: 'v-side',
  template: `
<md-card class="sidebar">
    <md-list>
  <h3 md-subheader>My workspace</h3>
  <md-list-item *ngFor="let folder of folders">
    <md-icon md-list-icon>folder</md-icon>
    <h4 md-line>{{folder.name}}</h4>
  </md-list-item>
  <h3 md-subheader>Notes</h3>
  <md-list-item *ngFor="let note of notes">
    <md-icon md-list-icon>note</md-icon>
    <h4 md-line>{{note.name}}</h4>
  </md-list-item>
</md-list>
</md-card>`
})
export class SideComponent  {
  folders = [
    {
      name: 'Dashboard',
    },
    {
      name: 'User Profile',
    },
    {
      name: 'Workspace',
    }
  ];
  notes = [
    {
      name: 'Vacation Itinerary',
      updated: new Date('2/20/16'),
    },
    {
      name: 'Kitchen Remodel',
      updated: new Date('1/18/16'),
    }
  ];
}
