import { Component } from '@angular/core';

@Component({
  selector: 'v-header',
  template: `
<md-card class="header">
  <md-toolbar>
     <h1>Verifika</h1>
  </md-toolbar>
</md-card>`
})
export class HeaderComponent  {  }
