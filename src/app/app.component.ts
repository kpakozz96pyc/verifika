import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<div class="grid-container">
  <v-header  id="pageHeader"></v-header>
  <v-content id="content"></v-content>
  <v-side id="side"></v-side>
  <v-footer id="pageFooter">Footer</v-footer>
  </div>
`
})
export class AppComponent  { name = 'Angular'; }
